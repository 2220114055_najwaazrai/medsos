from selenium.webdriver.common.by import By
from selenium import webdriver
import time


class Scraper:
  def __init__(self):
    self.driver = webdriver.Chrome()

  def get_data(self,keyword):
    self.driver.get('https://www.tokopedia.com/search?navsource=&ob=5&srp_component_id=02.01.00.00&srp_page_id=&srp_page_title=&st=product&q='+keyword)
    
    counter_page = 0
    datas = []

    while counter_page < 3:
      for _ in range(0, 6500, 500):
        time.sleep(0.1)
        self.driver.execute_script("window.scrollBy(0,500)")

      # elements = self.driver.find_elements(by=By.CLASS_NAME, value='css-974ipl')
      # for element in elements:
      #   #img = element.find_element(by=By.CLASS_NAME, value='css-1c345mg').get_attribute('src')
      #   name = element.find_element(by=By.CLASS_NAME, value='prd_link-product-name css-svipq6').text
      #   price = element.find_element(by=By.CLASS_NAME, value='prd_link-product-price css-1ksb19c').text
      #   city = element.find_element(by=By.CLASS_NAME, value='').text

        # datas.append({
        #   'img': img,
        #   'name': name,
        #   'price': price,
        #   'city': city
        # })
      names = self.driver.find_elements(by=By.CLASS_NAME, value='css-svipq6')
      prices = self.driver.find_elements(by=By.CLASS_NAME, value='css-1ksb19c')
      imgs = self.driver.find_elements(by=By.CLASS_NAME,value='css-1c345mg')
      cities = self.driver.find_elements(by=By.CLASS_NAME,value='css-1kdc32b')
      #city = self.driver.find_elements(by=By.CLASS_NAME,)
      for x in range(len(names)):
        name = names[x].text
        price = prices[x].text
        img = imgs[x].get_attribute('src')
        city = cities[x].text
        datas.append({
          'img': img,
          'name': name,
          'price': price,
          'city': city
        })

      # for element in elements:
      #   name = element.text
      #   datas.append(name)
      counter_page += 1
      #next_page = self.driver.find_element(by=By.XPATH, value="//a[@class='css-1eamy6l-unf-pagination-item']")
      #next_page = self.driver.find_elements(by=By.CLASS_NAME,value='unf-icon')
      #t = next_page[-1]
      #t.click()
      
      next_page = self.driver.find_element(by=By.XPATH, value="//button[@class='css-1ix4b60-unf-pagination-item' and text()='" + str(counter_page + 1) + "']")
      self.driver.execute_script("arguments[0].click();", next_page)
    
    return datas