# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 12:05:21 2022

@author: PIHOUSE
"""
from datetime import datetime
import streamlit as st
import pandas as pd
import numpy as np
import time
from PIL import Image
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib.pyplot as plt
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
stop_factory = StopWordRemoverFactory()

if "visibility" not in st.session_state:
    st.session_state.visibility = "visible"
    st.session_state.disabled = False

def space(num_lines=1):
    """Adds empty lines to the Streamlit app."""
    for _ in range(num_lines):
        st.write("")
st.set_page_config(layout="centered", page_title="sns driven products app")

#tw_image = np.array(Image.open("C:/Users/ASUS/Pictures/google.png"))

#topics = ["alat olahraga","alat yoga", "sneakers ventela", "iphone14","sneakers compass"]
#text_input = st.selectbox("Pilih topik...",topics)
space(1)
# CSS to inject contained in a string
hide_table_row_index = """
            <style>
            thead tr th:first-child {display:none}
            tbody th {display:none}
            </style>
            """

# Inject CSS with Markdown
st.markdown(hide_table_row_index, unsafe_allow_html=True)

#toped = pd.read_csv("D:/TSDN22/tokopedia.csv")
#shopi = pd.read_csv("D:/TSDN22/shopi.csv")
    
tweets = pd.read_csv("sneakers_tw.csv")
tweets
#tweets.columns =['username', 'content', 'location']
#st.table(tweets)
# if text_input :
#     with st.spinner('Wait for it...'):
#         tweets = tweets[tweets['content'].str.contains(text_input)] 
#         st.header("Total "+str(tweets.shape[0])+" tweets")
#         st.table(tweets.head())

def cloud(image, text, max_word, max_font, random):
    stopwords = stop_factory.get_stop_words()
    
    wc = WordCloud(background_color="white", colormap="hot", max_words=max_word, mask=image,
    stopwords=stopwords, max_font_size=max_font, random_state=random)

    # generate word cloud
    wc.generate(text)

    # create coloring from image
    #image_colors = ImageColorGenerator(image)

    # show the figure
    plt.figure(figsize=(100,100))
    fig, axes = plt.subplots(1,2, gridspec_kw={'width_ratios': [3, 2]})
    axes[0].imshow(wc, interpolation="bilinear")
    # recolor wordcloud and show
    # we could also give color_func=image_colors directly in the constructor
    axes[1].imshow(image, cmap=plt.cm.gray, interpolation="bilinear")

    for ax in axes:
        ax.set_axis_off()
    st.pyplot()
    #tweets["content"].str.cat(sep=' ')

#st.write(cloud(tw_image, tweets["content"].str.cat(sep=' '), 20, 22, 2), use_column_width=True)