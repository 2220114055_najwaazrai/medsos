#Importing Libraries

import pandas as pd
import matplotlib.pyplot as plt
from wordcloud import WordCloud
import json
import streamlit as st
import snscrape.modules.twitter as twt
import json 
import html
import re
import nltk
import string
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
from cleantext import clean 
from scraper import Scraper
import json

st.write("""
#  Analisis Data Twitter dan Marketplace
# Team Good Job
# """)
#Search 
spell = pd.read_csv("https://raw.githubusercontent.com/nasalsabila/kamus-alay/master/colloquial-indonesian-lexicon.csv")
spell = spell[['slang','formal']]
slang_replace = dict(spell.values)
stop_factory = StopWordRemoverFactory()
data = stop_factory.get_stop_words()
stopword = stop_factory.create_stop_word_remover()
data.remove('tidak')
st.set_option('deprecation.showPyplotGlobalUse', False)
def listToString(s):
   
    # initialize an empty string
    str1 = " "
   
    # return string 
    return (str1.join(s))
st.text_input("Search Here", key="search")
# You can access the value at any point with:
k = st.session_state.search
if st.button('Search'):
    st.write("jalan")
    scraper = twt.TwitterSearchScraper(k)
    fill=[]
    text = []
    for i,tweet in enumerate(scraper.get_items()):
        if i>10:
            break
        #x = tweet.content.replace("\n"," ")
        #tweet.content = html.unescape(x)
        tweet.content = re.sub(r"(@[A-Za-z0–9_]+)|[^\w\s]|#|http\S+", "", tweet.content)
        tweet.content = clean(tweet.content,fix_unicode=True,               # fix various unicode errors
                                to_ascii=True,                  # transliterate to closest ASCII representation
                                lower=True,                     # lowercase text
                                no_line_breaks=True,           # fully strip line breaks as opposed to only normalizing them
                                no_urls=True,                  # replace all URLs with a special token
                                no_emails=False,                # replace all email addresses with a special token
                                no_phone_numbers=False,         # replace all phone numbers with a special token
                                no_numbers=False,               # replace all numbers with a special token
                                no_digits=False,                # replace all digits with a special token
                                no_currency_symbols=True,      # replace all currency symbols with a special token
                                no_punct=False,                 # remove punctuations
                                replace_with_punct="",          # instead of removing punctuations you may replace them
                                replace_with_url="<URL>",
                                replace_with_currency_symbol="<CUR>",)
        x = tweet.content
        tweet.content = " ".join(slang_replace.get(word, word) for word in x.split(' '))
        fill.append([tweet.user.username, tweet.content,tweet.user.location])
        tweets_to_token = word_tokenize(tweet.content)
        tweets_to_token = [word for word in tweets_to_token if not word in data]
        text.append(tweets_to_token)
    df = pd.DataFrame(fill)
    df.columns = ['username','tweet','location']
    df = df.drop_duplicates('tweet',keep='first')
    df
    #flat = [food for sublist in text for food in sublist]
    flat = df['tweet']
    flat = listToString(flat)
    wordcloud = WordCloud().generate(flat)
    # Display the generated image:
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    plt.show()
    st.pyplot()
    scraper = Scraper()
    datas = scraper.get_data(k)
    index = 1
    tokped = pd.DataFrame(datas)
    tokped.columns=['img','nama_produk','price','kota']
    tokped
    #print(datas)
    # f = open("kopi.json","w")
    # j = json.dumps(datas)
    # f.write(j)
    # f.close()
    # for data in datas:
    # print(
    #   index,
    #   data['name'],
    #   data['img'],
    #   data['price'],
    #   data['city']
    # )

    #index += 1

    # f = open("alatolahraga_tw.json","w")
    # j = json.dumps(fill)
    # f.write(j)
    # f.close()
    # df_tw = pd.read_json('alatolahraga_tw.json')
    # newdata = df_tw.drop_duplicates(1,keep='first')
    # newdata
    #flat = [food for sublist in dup for food in sublist]
    #flat
    #kemunculan = FreqDist(flat)
    #kemunculan.plot(30,cumulative=False)
    #arr = np.random.normal(1, 1, size=100)
    #fig, ax = plt.subplots()
    #ax.hist(kemunculan.most_common(), bins=20)
    #st.pyplot(fig)
    #st.pyplot(fig)
    #st.line_chart(kemunculan.most_common())
else :
    st.write("hem")
#Creating the text variable
##text2 = " ".join(title for title in df.title)
# Creating word_cloud with text as argument in .generate() method
##word_cloud2 = WordCloud(collocations = False, background_color = 'white').generate(text2)

# Display the generated Word Cloud

##plt.imshow(word_cloud2, interpolation='bilinear')

##plt.axis("off")

##plt.show()