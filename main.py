import streamlit as st
import pandas as pd 
import numpy as np
import snscrape.modules.twitter as twt
import json 
import html
import re
import nltk
import string
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
import matplotlib.pyplot as plt
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory

stop_factory = StopWordRemoverFactory()
data = stop_factory.get_stop_words()
stopword = stop_factory.create_stop_word_remover()
data.remove('tidak')

st.write("""
# # My First App
# Hello *World!*
# """)
df = pd.DataFrame({
  'first column': [1, 2, 3, 4],
  'second column': [10, 20, 30, 40]
})
df
dataframe = pd.DataFrame(
    np.random.randn(10, 5),
    columns=('col %d' % i for i in range(5)))
st.table(dataframe)
x = [1,2,3]
st.write("hi",x[0]+1)


chart_data = pd.DataFrame(
     np.random.randn(10, 3),
     columns=['a', 'b', 'c'])
st.line_chart(chart_data)

map_data = pd.DataFrame(
    np.random.randn(1000, 2) / [50, 50] + [37.76, -122.4],
    columns=['lat', 'lon'])

st.map(map_data)
x = st.slider('x')  # 👈 this is a widget
st.write(x, 'squared is', x * x)



st.text_input("Search Here", key="search")

# You can access the value at any point with:
k = st.session_state.search
k
if st.button('Search'):
    st.write("jalan")
    scraper = twt.TwitterSearchScraper(k)
    fill=[]
    dup = []
    for i,tweet in enumerate(scraper.get_items()):
        if i>1000:
            break
        x = tweet.content.replace("\n"," ")
        tweet.content = html.unescape(x)
        tweet.content = re.sub(r"(@[A-Za-z0–9_]+)|[^\w\s]|#|http\S+", "", tweet.content)
        tweets_to_token = tweet.content
        tweets_to_token = word_tokenize(tweet.content)
        tweets_to_token = [word for word in tweets_to_token if not word in data]
        dup.append(tweets_to_token)
        fill.append([tweet.user.username, tweet.content,tweet.user.location])
    f = open("alatolahraga_tw.json","w")
    j = json.dumps(fill)
    f.write(j)
    f.close()
    df_tw = pd.read_json('alatolahraga_tw.json')
    newdata = df_tw.drop_duplicates(1,keep='first')
    newdata
    #flat = [food for sublist in dup for food in sublist]
    #flat
    kemunculan = FreqDist(flat)
    #kemunculan.plot(30,cumulative=False)
    #arr = np.random.normal(1, 1, size=100)
    #fig, ax = plt.subplots()
    #ax.hist(kemunculan.most_common(), bins=20)
    #st.pyplot(fig)
    #st.pyplot(fig)
    st.line_chart(kemunculan.most_common())
else :
    st.write("hem")

kalimat = "Andi kerap melakukan transaksi rutin secara daring atau online. Menurut Andi belanja online lebih praktis & murah."
kalimat = kalimat.translate(str.maketrans('','',string.punctuation)).lower()
tokens = word_tokenize(kalimat)
tokens
search = pd.read_json("sneakers_tw.json")
search


